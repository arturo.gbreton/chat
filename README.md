Primer proyecto.

Para construir el proyecto y ejecutar el programa, ejecute los siguientes comandos:

`git clone https://gitlab.com/arturo.gbreton/chat`

`cd chat/`

`meson setup builddir`

`cd builddir`

`ninja`

`./servidor`

`./cliente`
